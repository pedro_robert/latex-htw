% vim: filetype=tex
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{htw}[2016/10/09 Roberts HTW class]

\LoadClass[12pt,a4paper,twoside]{article}

\RequirePackage[margin=1.2in]{geometry}
\RequirePackage{etoolbox}

% \RequirePackage{subfiles} % modular document

\RequirePackage{graphicx}
\RequirePackage{epstopdf}
\graphicspath{ {res/} }

\RequirePackage{float}
\RequirePackage[font=footnotesize,labelfont=bf]{caption}
\RequirePackage{subcaption}
\RequirePackage{bookmark}

\RequirePackage{color}
\RequirePackage{xcolor}
\definecolor{htwgreen}{HTML}{76B900}

\RequirePackage[german=quotes]{csquotes}

\RequirePackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,
    citecolor=yellow,
    filecolor=blue,
    urlcolor=htwgreen,
    }

%% Spelling
\RequirePackage{polyglossia}
\setdefaultlanguage[spelling=new]{german}

%% Font settings
\RequirePackage{fontspec} % font features
\defaultfontfeatures{Ligatures=TeX} % To support LaTeX quoting style
    \setmainfont{Noto Serif}
    \setsansfont{Noto Sans}
    \setmonofont{Iosevka Light}
    % \setmathfont[Digits,LatinhGreek]{}

%% Code
\RequirePackage{minted}
\setminted{
  fontsize=\footnotesize,
  frame=single,
  framesep=8pt,
  numbersep=4pt,
  baselinestretch=1.2,
  linenos
}
\renewcommand{\listoflistingscaption}{Quellcodezeichnis}
\renewcommand{\listingscaption}{Code}

\RequirePackage{listings} % code hilighting
\RequirePackage{listings-golang} % ^ add support for go code
\lstset{
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
  %title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
  rulecolor=\color{black},
  keywordstyle=\color{htwgreen},
  commentstyle=\color{gray},
  numberstyle=\tiny\color{gray},
  stringstyle=\color{gray},
}

%% Headers
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}
\setlength{\headheight}{15.2pt}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\small \@htw@course}
\fancyhead[LO]{\small \@htw@title}
\fancyhead[RE]{\small \HTWBerlin}
\fancyhead[RO]{\small \@htw@topic}

\fancyfoot{}
\fancyfoot[RE,LO]{\small \@htw@footer}
\fancyfoot[LE,RO]{\small \thepage \hspace{1pt} /\hspace{1pt} \pageref{LastPage}}

\renewcommand{\headrulewidth}{0.4pt}
% \renewcommand{\footrulewidth}{0.4pt}

%% Custom commands
\newcommand\HTWBerlin{Hochschule für Technik und Wirtschaft Berlin}
% variables
% authors
\newcounter{cnt}
\newcommand\htwAddAuthor[2]{
    \stepcounter{cnt}
    \csdef{htw@author@first@\thecnt}{#1}
    \csdef{htw@author@last@\thecnt}{#2}
}
\newcommand\htwGetAuthor[1]{
    \csuse{htw@author@first@#1} \csuse{htwAuthor@last@#1}
}
\newcounter{rownum}
\newcommand\makeHtwAuthorList{
    \setcounter{rownum}{0}
    \whileboolexpr
        { test {\ifnumcomp{\value{rownum}}{<}{\thecnt}} }
        {
            \stepcounter{rownum}
            {\large
                \csuse{htw@author@first@\therownum}
                \textbf{\csuse{htw@author@last@\therownum}}
            }\\[0.3cm]
        }
}

\newcommand\htwCourse[1]{\def\@htw@course{#1}}
\newcommand\htwTopic[1]{\def\@htw@topic{#1}}
\newcommand\htwTitle[1]{\def\@htw@title{#1}}
\newcommand\htwSubtitle[1]{\def\@htw@subtitle{#1}}
\newcommand\htwDate[1]{\def\@htw@date{#1}}
\newcommand\htwFooter[1]{\def\@htw@footer{#1}}

% titlepage
\newcommand{\maketitlepage}{%

    \begin{figure}
        \begin{subfigure}{0.5\textwidth}

            \includegraphics[trim= 7mm 2mm 7mm 4mm,clip,height=1.5cm]{htw.eps}
            \hfill

        \end{subfigure}
        \begin{subfigure}{0.5\textwidth}

            \hfill
            \includegraphics[height=1.5cm]{ce.eps}

        \end{subfigure}
    \end{figure}

    %\vskip 60\p@

    \null\vfill
    \begin{center}

        \textsf{\textsc{\Large \@htw@course}}\\[1.5cm]
        {\Large \@htw@topic}\\[0.5cm]
        {\large \@htw@date}\\[2cm]
        {\textbf{\Huge \@htw@title}}\\[0.5cm]
        {\large \@htw@subtitle}

    \end{center}
    \null\vfill

    % Multiple Authors
    \begin{flushleft}
        \makeHtwAuthorList
    \end{flushleft}
    \@thanks
    \vfil\null
    \end{titlepage}
}
\renewcommand\maketitle{
    \begin{titlepage}
        \let\footnotesize\small
        \let\footnoterule\relax
        \let \footnote \thanks
        \maketitlepage
        \setcounter{footnote}{0}
        \global\let\thanks\relax
        \global\let\maketitle\relax
        \global\let\@thanks\@empty
        \global\let\@author\@empty
        \global\let\@date\@empty
        \global\let\@title\@empty
        \global\let\title\relax
        \global\let\author\relax
        \global\let\date\relax
        \global\let\and\relax
}
